#!/bin/bash

echo "Pegar ônibus é bom
Você só vai vencer amanhã se não desistir hoje
Respire fundo e continue, é proibido desistir
A meta é um sonho com um prazo
Sonhos são portas, atitude é a chave
Quem acredita sempre alcança
Se não puder fazer tudo, faça tudo que puder
Quanto mais você se supera, mais motivado fica
Quando todos avançam juntos, o sucesso ocorre por si só
Quando a gente acha que tem as respostas, vem a vida e muda todas as perguntas"
