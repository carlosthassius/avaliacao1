#!/bin/bash

hostname -I
ip addr
uname -a
free
free -h
df
fdisk -l
lscpu
whoami
vmstat
iptraf
find / -type f | wc -l

